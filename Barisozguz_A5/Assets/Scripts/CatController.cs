using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class CatController : MonoBehaviour
{
    public NavMeshAgent cat;
    public Animator CatAnim;
    public ARPlacement arp;

    //audio
    public AudioSource walking;
    public AudioSource purring;
    public AudioSource madgeNoise;
    bool isCoroutineReady = true;

    // Update is called once per frame
    void Update()
    {
        //makes the cat walk over to the selected position by using bool and referencing to ARPlacement Script
       if(arp.didHit == true)
       {
            cat.SetDestination(arp.PlacementPose.position);
       }

       if(cat.velocity != Vector3.zero)
       {
            CatAnim.SetBool("isWalking", true);
            walking.Play();
       }
       else if (cat.velocity == Vector3.zero)
       {
            CatAnim.SetBool("isWalking", false);
            walking.Stop();
            arp.didHit = false;
       }
       // makes the cat inverted on double cat so you can see her belly
       if (Input.touchCount == 2)
       {
            if (isCoroutineReady)
            {
                isCoroutineReady = false;
                StartCoroutine(PlayMadgeNoise());
            }
        }

       // touch and hold detection for purring state

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            
            if (touch.phase == TouchPhase.Stationary)
            {
                purring.Play(); // audio play
            }
            if (touch.phase == TouchPhase.Ended)
            {
                purring.Stop(); // stops when hold is released
            }

        }
    }

    IEnumerator PlayMadgeNoise()
    {
        madgeNoise.Play();

        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(5);

        madgeNoise.Stop();
        isCoroutineReady = true;
    }

}
